import json
import pandas as pd
import numpy as np
import datetime
import os
from sklearn.linear_model import LinearRegression

# Data: https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0
# Raw data source : https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson
def loadData():
    casesFile = open(os.path.dirname(__file__) + "/RKI_COVID19.geojson", "r")
    casesData = json.load(casesFile)

    return casesData

def getFeatures(data):
    completeList = data['features']
    return list(map(lambda entry: entry['properties'], completeList))

def getDataFrame(features):
    return pd.DataFrame.from_dict(features)

def getLandkreise(dataFrame):
    return list(dataFrame['Landkreis'].unique())


def getIdLandkreise(dataFrame):
    return list(dataFrame['IdLandkreis'].unique())

# Some statistics information for validation
def simpleStatistics(dataFrame):
    landkreise = dataFrame['Landkreis'].unique()
    print("Anzahl Landkreise: ", len(landkreise))
    totalCases = dataFrame['AnzahlFall'].sum()
    print("Total cases: ", totalCases)
    totalDeaths = dataFrame['AnzahlTodesfall'].sum()
    print("Total deaths: ", totalDeaths)

def getAverages(dataFrame): 
    regionalData = list(dataFrame.groupby('Landkreis'))
    averages = {}
    currentDay = datetime.date.today()

    for (landkreis, data) in regionalData:
        averages[landkreis] = lastDaysAverage(data, beforeDay = currentDay)

    return averages

def lastDaysAverage(data, beforeDay, numberOfDays = 7):
    def inRange(reportingDateString):
        reportingDate = parseDate(reportingDateString)
        timeDifference = beforeDay - reportingDate
        return reportingDate <= beforeDay and timeDifference.days <= numberOfDays

    inTimePeriod = data['Meldedatum'].map(inRange)
    newCasesOverLastDays = data[inTimePeriod]

    return newCasesOverLastDays['AnzahlFall'].sum() / numberOfDays 

def parseDate(dateString):
    return datetime.date.fromisoformat(dateString.split('T')[0])

# Returns the number of cases that were already existing
# *numberOfDays* before
def casesUntilDaysBefore(regionalData, numberOfDays):
    currentDay = datetime.date.today()

    def inRange(reportingDateString):
        reportingDate = parseDate(reportingDateString)
        timeDifference = currentDay - reportingDate
        return timeDifference.days >= numberOfDays

    inTimePeriod = regionalData['Meldedatum'].map(inRange)
    newCasesOverLastDays = regionalData[inTimePeriod]

    return newCasesOverLastDays['AnzahlFall'].sum()

def getCasesEvolutionOverTime(data):
    maximalNumberDaysAgo = 30

    evolution = {}

    casesAgo = {}
    for numberDaysAgo in range(0, maximalNumberDaysAgo):
        casesAgo[numberDaysAgo] = {
            landkreis: casesUntilDaysBefore(regionalData, numberDaysAgo)
            for (landkreis, regionalData) in list(dataFrame.groupby('IdLandkreis'))
        }

    for landkreis in getIdLandkreise(data):
        evolution[landkreis] = pd.Series(
            [casesAgo[numberDaysAgo][landkreis]
             for numberDaysAgo in range(0,maximalNumberDaysAgo)]
        )

    return evolution

## Forecasting
from statsmodels.tsa.arima_model import ARIMA

# Parameter: *casesSeries* a time series from the evolution
def forecastSingleARIMA(casesSeries, daysOfForecast=10):
    # *casesSeries* is going backwards in time
    # Instead: go from negativ indices to 0
    seriesFromPast = pd.Series(list(casesSeries)[::-1])
    print(seriesFromPast)
    model = ARIMA(seriesFromPast, order=(1, 1, 0))
    #try:
    model_fit = model.fit(disp = False)
    # always cumulative, so subtract current number of cases for prediction
    predictionCumulative = model_fit.predict(
        start = len(seriesFromPast),
        end = len(seriesFromPast) + daysOfForecast
    )
    print("predictionCumulative: ", predictionCumulative)

    predictedNewCases = predictionCumulative- seriesFromPast[len(seriesFromPast)-1]

    return list(predictedNewCases)
    #except:
    #    return []

def forecastSingleRegression(casesSeries, daysOfForecast=10):
    seriesFromPast = pd.Series(list(casesSeries)[::-1])

    dataSinceCases = seriesFromPast[seriesFromPast > 0]
    #print(dataSinceCases)

    logarithmicData = dataSinceCases.apply(np.log)

    # perform a linear regression of the logarithmic data
    x = np.array(range(0,len(logarithmicData))).reshape(-1,1)
    y = logarithmicData.values
    reg = LinearRegression().fit(
        x, y,
        sample_weight = np.arange(len(x))**4
    )

    cumulativeSoFar = dataSinceCases.values[len(dataSinceCases) - 1]

    predictions = [(np.exp(reg.predict(np.array(day).reshape(1,-1))) - cumulativeSoFar)[0] for day 
        in range(len(logarithmicData), len(logarithmicData) + daysOfForecast)]
    #print(predictions)
    return predictions
    

def forecastAll(evolution):
    return {landkreis: 
        forecastSingleRegression(evolution[landkreis])
        for landkreis in list(evolution.keys())
    } 
        
ratioNeedingIntensiveCareLowEstimate = 0.02
def getIntensiveCareForecastsLowEstimate(forecasts):
    return {
        landkreis: [ entry * ratioNeedingIntensiveCareLowEstimate for entry in forecasts[landkreis]]
        for landkreis in forecasts.keys()
    }

ratioNeedingIntensiveCareHighEstimate = 0.045
def getIntensiveCareForecastsHighEstimate(forecasts):
    return {
        landkreis: [ entry * ratioNeedingIntensiveCareHighEstimate for entry in forecasts[landkreis]]
        for landkreis in forecasts.keys()
    }

casesData = loadData()
features = getFeatures(casesData)
dataFrame = getDataFrame(features)
simpleStatistics(dataFrame)
#averages = getAverages(dataFrame)

evolution = getCasesEvolutionOverTime(dataFrame)
forecasts = forecastAll(evolution)



predictionFile = open(os.path.dirname(__file__) + "/prediction.json", "w")
json.dump(forecasts, predictionFile)

forecastsNeededIntensiveCareLowEstimate = getIntensiveCareForecastsLowEstimate(forecasts)
predictionIntensiveCareFileLowEstimate =  open(os.path.dirname(__file__) + "/predictionIntensiveCareLowEstimate.json", "w")
json.dump(forecastsNeededIntensiveCareLowEstimate, predictionIntensiveCareFileLowEstimate)

forecastsNeededIntensiveCareHighEstimate = getIntensiveCareForecastsHighEstimate(forecasts)
predictionIntensiveCareFileHighEstimate =  open(os.path.dirname(__file__) + "/predictionIntensiveCareHighEstimate.json", "w")
json.dump(forecastsNeededIntensiveCareHighEstimate, predictionIntensiveCareFileHighEstimate)


print("Total forecast for next day: ", sum(forecast[0] for forecast in  forecasts.values()))
print("Total additional intensive care for next day low estimate: ",
 sum(forecast[0] for forecast in forecastsNeededIntensiveCareLowEstimate.values()))
print("Total additional intensive care for next day high estimate: ",
 sum(forecast[0] for forecast in forecastsNeededIntensiveCareHighEstimate.values()))