import json
import pandas as pd
from datetime import datetime
import os
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
from random import random
import numpy as np

# Data: https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0
# Raw data source : https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson
def loadData():
    casesFile = open(os.path.dirname(__file__) + "/RKI_COVID19.geojson", "r")
    casesData = json.load(casesFile)

    return casesData

def getFeatures(data):
    completeList = data['features']
    return list(map(lambda entry: entry['properties'], completeList))

def getDataFrame(features):
    return pd.DataFrame.from_dict(features)

# Some statistics information for validation
def simpleStatistics(dataFrame):
    landkreise = dataFrame['Landkreis'].unique()
    print("Anzahl Landkreise: ", len(landkreise))
    totalCases = dataFrame['AnzahlFall'].sum()
    print("Total cases: ", totalCases)
    totalDeaths = dataFrame['AnzahlTodesfall'].sum()
    print("Total deaths: ", totalDeaths)

def analyzeData(dataFrame): 
    regionalData = list(dataFrame.groupby('Landkreis'))
    averages = {}
    currentDay = datetime.date.today()
    for (landkreis, data) in regionalData:
        averages[landkreis] = lastDaysAverage(data, beforeDay = currentDay)

    return averages

    # (landkreis, exampleData) = regionalData[1]
    # print(exampleData)
    # print(exampleData['Meldedatum'])

def parseDate(dateString):
    return datetime.fromisoformat(dateString.split('T')[0]).strftime('%Y%m%d')

# Noch nicht fertig
def lastDaysAverage(data, beforeDay, numberOfDays = 7):
    def inRange(reportingDateString):
        reportingDate = parseDate(reportingDateString)
        timeDifference = beforeDay - reportingDate

        return reportingDate <= beforeDay and timeDifference.day <= numberOfDays

    newCasesOverLastDays = data[data.apply(lambda entry: inRange(entry['Meldedatum']))]

    return newCasesOverLastDays / numberOfDays 

casesData = loadData()
features = getFeatures(casesData)
dataFrame = getDataFrame(features)
simpleStatistics(dataFrame)
#analyzeData(dataFrame)

def mask(df, key, value):
    return df[df[key] == value]


def aggregateCases(dataframe, filteropt = 'all',filterval = 'all'):
    aggregatedDataframe = pd.DataFrame()
    if filteropt != 'all':
        dataframe = mask(dataframe,filteropt,filterval)
    
    dataframe.sort_values('Meldedatum')
    dataframe['Meldedatum'] = pd.to_datetime(dataframe["Meldedatum"]).dt.strftime('%Y-%m-%d')
    aggregatedDataframe['dailyCases'] = dataframe.groupby('Meldedatum')['AnzahlFall'].sum()
    aggregatedDataframe['dailyCasescumsum'] = aggregatedDataframe['dailyCases'].cumsum()
    aggregatedDataframe['dailyDeaths'] = dataframe.groupby('Meldedatum')['AnzahlTodesfall'].sum()
    aggregatedDataframe['dailyDeathscumsum'] = aggregatedDataframe['dailyCases'].cumsum()    
    return aggregatedDataframe


def visulaizeTimeSeries(series, title = 'Cases'):
    plt.figure()
    series.plot(label=series.name)
    plt.xlabel('Time')
    plt.ylabel(series.name)
    
def visulaizeTimeSerieswithPred(series,pred, title = 'Cases'):
    plt.figure()
    plt.plot(np.arange(len(series)),series)
    plt.plot(np.arange(len(pred))+len(series),pred,'r')
    plt.xlabel('Time')
    plt.ylabel(series.name)
    
def Arima(series, forcasttimestamps = 5): 
    # ARIMA example
    # contrived dataset
    # fit model
    model = ARIMA(series, order=(2, 2, 1))
    model_fit = model.fit(disp=False)
    # make prediction
    yhat = model_fit.predict(len(series), len(series)+forcasttimestamps, typ='levels')
    print(yhat)
    return yhat


# for bundesland in (dataFrame['Bundesland'].unique()):
#     df_aggregated = aggregateCases(dataFrame, filteropt = 'Bundesland',filterval = bundesland)
#     y_pred = Arima(df_aggregated['dailyCasescumsum'])
#     visulaizeTimeSerieswithPred(df_aggregated['dailyCasescumsum'],y_pred, title = bundesland)

    
df_aggregated = aggregateCases(dataFrame, filteropt = 'Bundesland',filterval = 'Bayern')
y_pred = Arima(df_aggregated['dailyCasescumsum'])
visulaizeTimeSerieswithPred(df_aggregated['dailyCasescumsum'],y_pred, title = 'Bayern')
    



