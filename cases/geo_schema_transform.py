import json
import requests
from datetime import datetime
import argparse

ap = argparse.ArgumentParser()

ap.add_argument("-f", "--file", action='store', required=True, type=str, help="path of the file to parse")
args = ap.parse_args()

try:
    with open(args.file, 'r') as f:
        data = json.load(f)

        geoJsonObj = {
            "type": "FeatureCollection",
            "crs": {
                "type": "name",
                "properties": {
                    "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
                }
            }
        }

        geoFeatures = []

        for key in data.keys():
            print(key)
            res = requests.get(f'https://public.opendatasoft.com/api/records/1.0/search/?dataset=landkreise-in-germany&rows=1&facet=name_0&facet=name_1&facet=name_2&facet=type_2&facet=engtype_2&refine.cca_2={key}')
            api_res = json.loads(res.content)

            if "nhits" in api_res and api_res["nhits"] == 1:
                d_state_id = key
                d_name = api_res["records"][0]["fields"]["name_2"]
                d_state = api_res["records"][0]["fields"]["name_1"]
                d_geo = api_res["records"][0]["fields"]["geo_point_2d"]
                d_forcast = data[key]

                feature_obj = {
                    "type": "Feature",
                    "properties": {
                        "id": d_state_id,
                        "districtName": d_name,
                        "stateName": d_state,
                        "forcast": d_forcast
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [d_geo[1], d_geo[0], 0.0]
                    }
                }
                geoFeatures.append(feature_obj)

        geoJsonObj["features"] = geoFeatures

        with open("geo_" + args.file, "w") as outfile:
            json.dump(geoJsonObj, outfile)

except:
    print(f"Could not process file {args.file}")
